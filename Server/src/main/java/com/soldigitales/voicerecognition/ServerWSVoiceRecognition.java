/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soldigitales.voicerecognition;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.Microphone;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.result.WordResult;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * 1 Modelo - Palabra por palabra 
 * 
 * No existiría start, stop para campos abiertos. Cuando se diga el id de un Comando (ej. Menu_Alumno, Nombre, Botón Guardar, etc), se posicionaría ahí, las siguientes palabras se 
 * escribirían ahí (Ej. Jorge Jiménez) llendose por la opción "default" del switch (En el front-end se estaría sumando valores al campo abierto), si el usuario menciona de nuevo un "Comando", este programa lo identificaría y enviaría un 
 * elemento de comanndo al front-end
 * 
 * @author jorge.jimenez
 */

@ApplicationScoped
@ServerEndpoint("/voicerecognizer")
public class ServerWSVoiceRecognition {
    
    // Configuration Object
    Configuration configuration = new Configuration();
    LiveSpeechRecognizer recognize=null;
    //Microphone microphone = null;
    
    //String sbuffer=new StringBuilder();
    
    
    public ServerWSVoiceRecognition() throws IOException {
        
        
        System.out.println("Entra aqui Jorge - Costructor ServerWSVoiceRecognition");
        
        //Diccionario español
       //https://github.com/javierarce/palabras
       //https://github.com/olea/lemarios
       //http://giusseppe.net/blog/archivo/2015/10/29/diccionario-de-la-rae-en-modo-texto-plano/
       
        
        // Set path to the acoustic model. (Ingles)
        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        System.out.println("Entro 1");
        configuration.setDictionaryPath("C:/workspace/es_model/4296.dic");
        System.out.println("Entro 2");
        configuration.setLanguageModelPath("C:/workspace/es_model/4296.lm");
        System.out.println("Entro 3");
        
        //Español
        //http://www.speech.cs.cmu.edu/tools/lmtool-new.html  --> Herramienta para ingresar nuestros comandos
        //configuration.setAcousticModelPath("/Users/jorge.jimenez/Documents/Respaldo/Infotec/proyectos/PEI/2018/Total/EduManager/Jajs/VoiceRecognition/es_acousticModel");
        //configuration.setDictionaryPath("/Users/jorge.jimenez/Documents/Respaldo/Infotec/proyectos/PEI/2018/Total/EduManager/Jajs/VoiceRecognition/es_model/5319.dict");
        //configuration.setLanguageModelPath("/Users/jorge.jimenez/Documents/Respaldo/Infotec/proyectos/PEI/2018/Total/EduManager/Jajs/VoiceRecognition/es_model/4296.lm");
        
        

        //Modelo en español, obtenido de: https://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Models/
        //Y usado según:https://cmusphinx.github.io/wiki/tutorialadapt/#other-acoustic-models ---> Using the model
        //Liga que puede corregir mi problema: https://sourceforge.net/p/cmusphinx/discussion/sphinx4/thread/c3dbbfd4/#c521/2c6e
        //configuration.setAcousticModelPath("resource:/com/soldigitales/voicerecognition/es_acousticModel/");
        
       //http://www.speech.cs.cmu.edu/tools/lmtool-new.html  --> Herramienta para ingresar nuestros comandos
        
         //Recognizer Object, Pass the Configuration object
        recognize = new LiveSpeechRecognizer(configuration);
        
        
    }
    
 
    private Set<Session> sessions = new HashSet<>();
          
    @OnOpen
    public void open(Session session) {
        System.out.println("Session opened ==>");
        sessions.add(session);
    }
 
    @OnMessage
    public void handleMessage(String message, Session session) throws IOException {
        System.out.println("En ServerWS");
        
        
        
        String sValue=null;
        
        //Start Recognition Process (The bool parameter clears the previous cache if true)
        
        recognize.startRecognition(true);
        
        //Create SpeechResult Object
        SpeechResult result;
        
        try {
            //Checking if recognizer has recognized the speech
            System.out.println("GenString:"+recognize.getClass().toGenericString());
            while ((result = recognize.getResult()) != null) {
                //Get the recognize speech
                System.out.println("Resultado crudo7:"+result.getResult());
                
                String command = result.getHypothesis().toLowerCase();
                
                System.out.println("CommandJ:"+command);

                 switch (command) {

                    case "open file manager":
                        System.out.println("File Manager Opened!");
                        sValue=command;
                        break;
                    case "close file manager":
                        System.out.println("File Manager Closed!");
                        sValue=command;
                        break;
                    case "open browser":
                       System.out.println("Browser Opened!");
                       sValue=command;
                       break;
                    case "close browser":
                        System.out.println("Browser Closed!");
                        sValue=command;
                        break;
                    default:
                        System.out.println("commando no reconocido:"+command);
                        sValue=command;
                        break; 
                }
                 System.out.println("Termina Loop principal...");
                 break;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Sale carnal...");
        try{
            recognize.stopRecognition();
        }catch(Exception e){
            
        }
        session.getBasicRemote().sendText("{\"value\" : \"" + sValue + "\"}");

        
    }
 
    @OnClose
    public void close(Session session) {
        System.out.println("Session closed ==>");
        sessions.remove(session);
    }
 
    @OnError
    public void onError(Throwable e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
    }
}
