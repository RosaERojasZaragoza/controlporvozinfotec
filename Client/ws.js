var socket = new WebSocket("ws://localhost:8080/VoiceRecognition-1.0-SNAPSHOT/voicerecognizer");
socket.onmessage = onMessage;

socket.onopen = function(event) {
   console.log("Connection established");
   // Display user friendly messages for the successful establishment of connection
   var label = document.getElementById("lblValue");
   label.innerHTML = "Connection established";
   socket.send("{\"start\":\"true\"}");
}

function onMessage(event) {

    console.log("En ClientWS");

    var data = JSON.parse(event.data);

    var lblValue = document.getElementById("lblValue");
    if(data.value != null){
        lblValue.innerHTML = 'Recibe: ' + data.value;
    }
    socket.send("{\"start\":\"true\"}");
}
